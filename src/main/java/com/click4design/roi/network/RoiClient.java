package com.click4design.roi.network;

import com.click4design.roi.model.Category;
import com.click4design.roi.model.Petition;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;
import retrofit.http.Query;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 19.06.13
 * Time: 10:51
 */
public interface RoiClient {
    public final static String API_URL = "http://roi.bettips.ru";

    @GET("/petitions")
    List<Petition> petitions(
            @Query("category") String category,
            @Query("sort") String sort,
            @Query("status") int status,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("/petitions")
    @Headers({
            "Content-Type: application/json"
    })
    List<Petition> petitions(
            @Query("sort") String sort,
            @Query("status") int status,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("/petitions")
    List<Petition> petitions(
            @Query("ids") String ids,
            @Query("sort") String sort,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("/categories")
    List<Category> categories();
}
