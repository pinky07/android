package com.click4design.roi.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Author: Khamidullin Kamil
 * Date: 19.06.13
 * Time: 15:45
 */
public class Petition implements Parcelable {
    protected String id;
    protected String title;
    protected String url;
    protected int votes;
    protected int create_date;
    protected int status;
    protected String description;
    protected String benefit;
    protected String number;
    protected int poll_expiration_date;
    protected String type;

    public final static int STATUS_POLL         = 1;
    public final static int STATUS_ADVISEMENT   = 2;
    public final static int STATUS_COMPLETE     = 3;

    public Petition(){}

    public Petition(Parcel parcel) {
        setId(parcel.readString());
        setTitle(parcel.readString());
        setVotes(parcel.readInt());
        setBenefit(parcel.readString());
        setDescription(parcel.readString());
        setNumber(parcel.readString());
        setCreateDate(parcel.readInt());
        setPollExpirationDate(parcel.readInt());
        setType(parcel.readString());
        setStatus(parcel.readInt());
        setUrl(parcel.readString());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public int getCreateDate() {
        return create_date;
    }

    public void setCreateDate(int createDate) {
        this.create_date = createDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBenefit() {
        return benefit;
    }

    public void setBenefit(String benefit) {
        this.benefit = benefit;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getPollExpirationDate() {
        return poll_expiration_date;
    }

    public void setPollExpirationDate(int pollExpirationDate) {
        this.poll_expiration_date = pollExpirationDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getId());
        parcel.writeString(getTitle());
        parcel.writeInt(getVotes());
        parcel.writeString(getBenefit());
        parcel.writeString(getDescription());
        parcel.writeString(getNumber());
        parcel.writeInt(getCreateDate());
        parcel.writeInt(getPollExpirationDate());
        parcel.writeString(getType());
        parcel.writeInt(getStatus());
        parcel.writeString(getUrl());
    }

    public static final Parcelable.Creator<Petition> CREATOR = new Parcelable.Creator<Petition>() {
        public Petition createFromParcel(Parcel in) {
            return new Petition(in);
        }

        public Petition[] newArray(int size) {
            return new Petition[size];
        }
    };

    public String getTypeName() {
        if(getType().equals(PetitionType.FEDERAL))
            return "федеральный";
        else if(getType().equals(PetitionType.REGIONAL))
            return "региональный";
        else if(getType().equals(PetitionType.MUNICIPAL))
            return "муниципальный";
        else
            return "";
    }

    public static class PetitionType {
        public final static String FEDERAL = "federal";
        public final static String REGIONAL  = "regional";
        public final static String MUNICIPAL = "municipal";
    }
}
