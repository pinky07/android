package com.click4design.roi.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.click4design.roi.R;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockListFragment;

/**
 * Author: Khamidullin Kamil
 * Date: 19.06.13
 * Time: 9:44
 */
public class SlideMenuFragment extends RoboSherlockListFragment {
    protected MenuItem[] mMenuItems = new MenuItem[] {
            new MenuItem("На голосовании", R.drawable.ic_poll),
            new MenuItem("На рассмотрении", R.drawable.ic_advisement),
            new MenuItem("Решение принято", R.drawable.ic_complete),
            new MenuItem("Избранные", R.drawable.ic_favorite),
    };

    protected Fragment[] mSectionFragments;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSectionFragments = new Fragment[] {
                PollPetitionsWrapperFragment.newInstance(),
                PetitionsFragment.newInstance(mMenuItems[1].title, 2),
                PetitionsFragment.newInstance(mMenuItems[2].title, 3),
                PetitionsFragment.newInstance(mMenuItems[3].title, 0)};

        setListAdapter(new SlideMenuAdapter(getActivity(), mMenuItems));
    }

    private class MenuItem {
        public String title;
        public int iconRes;
        public MenuItem(String title, int iconRes) {
            this.title      = title;
            this.iconRes    = iconRes;
        }
    }

    public class SlideMenuAdapter extends ArrayAdapter<MenuItem> {

        public SlideMenuAdapter(Context context, MenuItem[] items) {
            super(context, 0);

            for (MenuItem item : items) {
                add(item);
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, null);
            }
            ((ImageView) convertView.findViewById(R.id.row_icon)).setImageResource(getItem(position).iconRes);
            ((TextView) convertView.findViewById(R.id.row_title)).setText(getItem(position).title);

            return convertView;
        }

    }

    @Override
    public void onListItemClick(ListView lv, View v, int position, long id) {
        switchFragment(mSectionFragments[position]);
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;

        if (getActivity() instanceof MainActivity) {
            MainActivity ra = (MainActivity) getActivity();
            ra.switchContent(fragment);
        }
    }
}
