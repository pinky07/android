package com.click4design.roi.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Author: Khamidullin Kamil
 * Date: 19.06.13
 * Time: 17:35
 */
public class PollPetitionsViewPagerAdapter extends FragmentPagerAdapter
{
    protected Fragment[] fragments;
    protected final String title = "На голосовании";

    public PollPetitionsViewPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments = new Fragment[]{
                PetitionsFragment.newInstance(title, 1, "votes"),
                PetitionsFragment.newInstance(title, 1, "createDate"),
        };
    }

    public int getCount() {
        return fragments.length;
    }

    public Fragment getItem(int position) {
        return fragments[position];
    }
}
