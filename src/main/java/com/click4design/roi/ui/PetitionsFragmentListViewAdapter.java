package com.click4design.roi.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.click4design.roi.R;
import com.click4design.roi.model.Petition;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 20.06.13
 * Time: 0:50
 */
public class PetitionsFragmentListViewAdapter extends BaseAdapter {
    protected List<Petition> mPetitions;
    protected LayoutInflater mInflater;
    protected Context mContext;
    protected final SimpleDateFormat petitionCreateDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public PetitionsFragmentListViewAdapter(Context context, List<Petition> petitions) {
        mContext   = context;
        mPetitions = petitions;
        mInflater  = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mPetitions.size();
    }

    @Override
    public Object getItem(int i) {
        return mPetitions.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = mInflater.inflate(R.layout.fragment_petitions_list_view_item, null);
        Petition petition = mPetitions.get(i);
        ((TextView)view.findViewById(R.id.petitionTitle)).setText(petition.getTitle());
        ((TextView)view.findViewById(R.id.petitionVotes)).setText(Integer.toString(petition.getVotes()));
        ((TextView)view.findViewById(R.id.petitionCreateDate)).setText(petitionCreateDateFormat.format(new Date((long)petition.getCreateDate()*1000)));
        ((TextView)view.findViewById(R.id.petitionJurisdiction)).setText(String.format("%s: %s", mContext.getString(R.string.jurisdiction_short), petition.getTypeName()));

        return view;
    }
}
