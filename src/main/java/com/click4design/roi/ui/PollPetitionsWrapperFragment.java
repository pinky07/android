package com.click4design.roi.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.MenuItem;
import com.click4design.roi.R;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockListFragment;
import roboguice.inject.InjectView;

import java.lang.reflect.Field;

/**
 * Author: Khamidullin Kamil
 * Date: 20.06.13
 * Time: 23:46
 */
public class PollPetitionsWrapperFragment extends RoboSherlockFragment implements ViewPager.OnPageChangeListener, ActionBar.TabListener {
    @InjectView(R.id.pollPetitionsPager)
    ViewPager pager;
    PollPetitionsViewPagerAdapter viewPagerAdapter;
    private String[] locations;

    public static PollPetitionsWrapperFragment newInstance() {
        return new PollPetitionsWrapperFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_poll_petitions, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return ((RoboSherlockListFragment)viewPagerAdapter.getItem(pager.getCurrentItem())).onOptionsItemSelected(item);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        locations = getResources().getStringArray(R.array.locations);
        configureActionBar();
        configureViewPager();
    }

    private void configureViewPager() {
        viewPagerAdapter = new PollPetitionsViewPagerAdapter(getChildFragmentManager());
        pager.setAdapter(viewPagerAdapter);
        pager.setOnPageChangeListener(this);
    }

    private void configureActionBar() {
        getSherlockActivity().getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        for (String location: locations) {
            ActionBar.Tab tab = getSherlockActivity().getSupportActionBar().newTab();
            tab.setText(location);
            tab.setTabListener(this);
            getSherlockActivity().getSupportActionBar().addTab(tab);
        }
    }

    @Override
    public void onPageSelected(int position) {
        ActionBar.Tab tab = getSherlockActivity().getSupportActionBar().getTabAt(position);
        getSherlockActivity().getSupportActionBar().selectTab(tab);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        int position = tab.getPosition();
        pager.setCurrentItem(position);
    }
    @Override
    public void onPageScrollStateChanged(int position) {}

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {}

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {}

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getSherlockActivity().getSupportActionBar().removeAllTabs();
        getSherlockActivity().getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }
}