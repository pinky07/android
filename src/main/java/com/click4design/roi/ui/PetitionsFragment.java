package com.click4design.roi.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.click4design.roi.R;
import com.click4design.roi.model.Category;
import com.click4design.roi.model.Petition;
import com.click4design.roi.persist.PersistHelper;
import com.click4design.roi.service.Requests;
import com.click4design.roi.service.RestRequestManager;
import com.foxykeep.datadroid.requestmanager.Request;
import com.foxykeep.datadroid.requestmanager.RequestManager;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockListFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 21.06.13
 * Time: 19:29
 */
public class PetitionsFragment extends RoboSherlockListFragment implements RequestManager.RequestListener, AbsListView.OnScrollListener {
    protected String mSort = "votes";
    protected int mStatus = 1;
    protected int mPage = 1;
    protected boolean isLastPage = false;
    protected String mTitle;
    protected String mCategory;

    static final int INTERNAL_EMPTY_ID = 16711681;
    protected boolean isloading = false;
    RequestManager requestManager;
    protected List<Petition> mPetitions = new ArrayList<Petition>();
    protected TextView mEmpty;

    public final static String EXTRA_SORT   = "sort";
    public final static String EXTRA_STATUS = "status";
    public final static String EXTRA_PAGE   = "page";
    public final static String EXTRA_TITLE   = "title";

    public static PetitionsFragment newInstance(String title, int status) {
        Bundle args = new Bundle();
        args.putString(EXTRA_TITLE, title);
        args.putInt(EXTRA_STATUS, status);

        PetitionsFragment fragment = new PetitionsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public static PetitionsFragment newInstance(String title, int status, String sort) {
        Bundle args = new Bundle();
        args.putString(EXTRA_TITLE, title);
        args.putInt(EXTRA_STATUS, status);
        args.putString(EXTRA_SORT, sort);

        PetitionsFragment fragment = new PetitionsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        super.onResume();
        requestManager = RestRequestManager.from(getActivity());
        update();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mStatus == 0 && getListAdapter() != null) {
            List<String> watchList = PersistHelper.getWatchList(getActivity());
            List<Petition> petitions = new ArrayList<Petition>();
            for(String id :watchList) {
                for(Petition petition : mPetitions) {
                    if(petition.getId().equals(id)) {
                        petitions.add(petition);
                    }
                }
            }
            mPetitions.clear();
            mPetitions.addAll(petitions);
            ((BaseAdapter)getListAdapter()).notifyDataSetChanged();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args.containsKey(EXTRA_SORT))
            mSort = args.getString(EXTRA_SORT);
        if(args.containsKey(EXTRA_STATUS))
            mStatus = args.getInt(EXTRA_STATUS);
        if(args.containsKey(EXTRA_PAGE))
            mPage = args.getInt(EXTRA_PAGE);

        mTitle = args.getString(EXTRA_TITLE);
        getSherlockActivity()
                .getSupportActionBar()
                .setTitle(mTitle);

        getSherlockActivity()
                .getSupportActionBar()
                .setSubtitle(null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setPadding(8, 8, 8, 8);
        ViewGroup rootView = (ViewGroup)getListView().getParent();

        mEmpty = (TextView)rootView.findViewById(android.R.id.empty);
        if(mEmpty == null) {
            mEmpty = (TextView)rootView.findViewById(INTERNAL_EMPTY_ID);
            mEmpty.setVisibility(View.GONE);
            mEmpty.setText("Инициативы отсутствуют");
        }
        getListView().setEmptyView(mEmpty);
        getListView().setDivider(getResources().getDrawable(R.drawable.list_view_divider));
        getListView().setBackgroundColor(getResources().getColor(R.color.background_color));
        getListView().setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), PetitionActivity.class);
                intent.putExtra(PetitionActivity.EXTRA_PETITION, mPetitions.get(i));
                startActivity(intent);
            }
        });
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.update) {
            update();
        } else {
            String[] categoriesIds = getResources().getStringArray(R.array.categories_ids);
            if(item.getItemId() >= 0 && item.getItemId() < categoriesIds.length) {
                if(item.getItemId() == 0) {
                    mCategory = null;
                    getSherlockActivity().getSupportActionBar().setSubtitle(null);
                } else {
                    mCategory = categoriesIds[item.getItemId()];
                    getSherlockActivity().getSupportActionBar().setSubtitle(item.getTitle());
                }

                update();
            }
        }
        return true;
    }

    protected void update() {
        if(isOnline(getSherlockActivity())) {
            mPage = 1;
            mPetitions.clear();
            setListAdapter(null);
            setListShown(false);
            loadPetitions(mPage);
        } else {
            Toast.makeText(
                    getSherlockActivity(),
                    "Отсутствует соединение с интернетом, проверьте настройки сети",
                    Toast.LENGTH_SHORT).show();
            setListAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1));
            mEmpty.setText("Нет соединения с интернетом");
        }

    }

    protected void loadPetitions(int page) {
        isloading = true;
        getSherlockActivity().setSupportProgressBarIndeterminateVisibility(true);
        Request updateRequest = new Request(Requests.PETITIONS);
        updateRequest.put("category", mCategory);
        updateRequest.put("limit", 10);
        updateRequest.put("status", mStatus);
        updateRequest.put("sort", mSort);
        updateRequest.put("offset", (page-1)*10);
        requestManager.execute(updateRequest, this);
    }

    @Override
    public void onRequestFinished(Request request, Bundle resultData) {
        if(getActivity() != null) {
            Parcelable[] parcelables = resultData.getParcelableArray("petitions");
            for(Parcelable petition : parcelables) {
                mPetitions.add((Petition)petition);
            }

            if(getListAdapter() == null) {
                setListAdapter(new PetitionsFragmentListViewAdapter(getActivity(), mPetitions));
                getListView().setOnScrollListener(this);
            } else {
                ((BaseAdapter)getListAdapter()).notifyDataSetChanged();
            }
            if(parcelables.length < 10) {
                isLastPage = true;
            } else {
                mPage++;
            }

            getSherlockActivity().setSupportProgressBarIndeterminateVisibility(false);
        }

        isloading = false;
    }

    @Override
    public void onRequestConnectionError(Request request, int statusCode) {
        onRequestError();
    }

    @Override
    public void onRequestDataError(Request request) {
        onRequestError();
    }

    @Override
    public void onRequestCustomError(Request request, Bundle resultData) {
        onRequestError();
    }

    protected void onRequestError() {
        if(getListAdapter() == null) {
            setListAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1));
        }
        if(getSherlockActivity() != null) {
            mEmpty.setText("Сервис временно недоступен, попробуйте позже");
            getSherlockActivity().setSupportProgressBarIndeterminateVisibility(false);
        }

        isloading = false;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {}

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        int loadedItems = firstVisibleItem + visibleItemCount;
        if(!isLastPage && (loadedItems == totalItemCount) && !isloading){
            loadPetitions(mPage);
        }
    }

    public boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return (netInfo != null && netInfo.isConnectedOrConnecting());
    }
}
