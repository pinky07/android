package com.click4design.roi.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;
import com.click4design.roi.R;
import com.click4design.roi.model.Petition;
import com.click4design.roi.persist.PersistHelper;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Author: Khamidullin Kamil
 * Date: 22.06.13
 * Time: 3:59
 */
@ContentView(R.layout.activity_petition)
public class PetitionActivity extends RoboSherlockActivity {
    public final static String EXTRA_PETITION = "petition";

    protected final SimpleDateFormat createDateFormat           = new SimpleDateFormat("dd MMM yyyy");
    protected final SimpleDateFormat pollExpirationDateFormat   = new SimpleDateFormat("dd.MM.yy");

    @InjectExtra(EXTRA_PETITION)
    protected Petition mPetition;

    @InjectView(R.id.votes)
    protected TextView mVotesView;

    @InjectView(R.id.description)
    protected TextView mDescriptionView;

    @InjectView(R.id.createDate)
    protected TextView mCreateDateView;

    @InjectView(R.id.title)
    protected TextView mTitle;

    @InjectView(R.id.jurisdiction)
    protected TextView mJurisdiction;

    @InjectView(R.id.pollExpirationDate)
    protected TextView mPollExpirationDate;

    @InjectView(R.id.benefit)
    protected TextView mBenefit;

    protected boolean isWatch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        getSupportActionBar().setTitle(mPetition.getTitle());
        getSupportActionBar().setSubtitle(String.format("Инициатива № %s", mPetition.getNumber()));

        mTitle.setText(mPetition.getTitle());
        if(mPetition.getType().equals(Petition.PetitionType.FEDERAL)) {
            mVotesView.setText(String.format("%d из 100 тыс голосов", mPetition.getVotes()));
        } else {
            mVotesView.setText(String.format("%d голосов", mPetition.getVotes()));
        }
        mCreateDateView.setText(String.format("Добавлена: %s", createDateFormat.format(new Date((long) mPetition.getCreateDate() * 1000))));
        mJurisdiction.setText(String.format("%s: %s", getString(R.string.jurisdiction), mPetition.getTypeName()));
        mDescriptionView.setText(mPetition.getDescription().replace("&#xD;\n", "\n").replaceAll("<(.*?)\\>", ""));
        mBenefit.setText(mPetition.getBenefit().replace("&#xD;\n", "\n").replaceAll("<(.*?)\\>",""));

        switch (mPetition.getStatus()) {
            case Petition.STATUS_POLL:
                mPollExpirationDate.setText(String.format("Голосование до %s", pollExpirationDateFormat.format(new Date((long)mPetition.getPollExpirationDate()*1000))));
                break;
            case Petition.STATUS_ADVISEMENT:
                mPollExpirationDate.setText("На рассмотрении");
                break;
            case Petition.STATUS_COMPLETE:
                mPollExpirationDate.setText("Решение принято");
                break;
        }

        isWatch = PersistHelper.isWatchPetition(this, mPetition);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        getSupportMenuInflater().inflate(R.menu.activity_petition, menu);
        MenuItem itemWatch = menu.findItem(R.id.watch);
        MenuItem share     = menu.findItem(R.id.share);
        itemWatch.setIcon(isWatch ? R.drawable.ic_action_unwatch : R.drawable.ic_action_watch);
        itemWatch.setTitle(isWatch ? "Отписаться" : "Следить");

        ShareActionProvider shareActionProvider = (ShareActionProvider) share.getActionProvider();
        shareActionProvider.setShareHistoryFileName(ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
        shareActionProvider.setShareIntent(createShareIntent());

        return super.onPrepareOptionsMenu(menu);
    }

    public Intent createShareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, mPetition.getTitle());
        intent.putExtra(Intent.EXTRA_TEXT, String.format("https://roi.ru/%s", mPetition.getUrl()));
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish(); break;
            case R.id.watch:
                toggleWatchPetition(mPetition);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void toggleWatchPetition(Petition petition) {
        if(isWatch) {
            PersistHelper.unWatch(this, petition);
            isWatch = false;
        } else {
            PersistHelper.watchPetition(this, petition);
            isWatch = true;
        }

        Toast toast = Toast.makeText(this, isWatch ?
                "Инициатива добавлена в избранные" :
                "Инициатива удалена из избранных", Toast.LENGTH_SHORT);
        //toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

        supportInvalidateOptionsMenu();
    }
}
