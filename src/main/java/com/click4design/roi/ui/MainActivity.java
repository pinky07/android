
package com.click4design.roi.ui;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import com.actionbarsherlock.view.Window;
import com.click4design.roi.R;
import com.click4design.roi.model.Category;
import com.click4design.roi.service.Requests;
import com.click4design.roi.service.RestRequestManager;
import com.foxykeep.datadroid.requestmanager.Request;
import com.foxykeep.datadroid.requestmanager.RequestManager;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

//@ContentView(R.layout.activity_main)
public class MainActivity extends SlidingFragmentActivity
{
    private Fragment mContent;
    protected boolean isHideOptionsMenu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.setSupportProgressBarIndeterminateVisibility(false);

        setContentView(R.layout.responsive_content_frame);
        setTitle(getString(R.string.app_name));

        //setSlidingActionBarEnabled(false);
        // check if the content frame contains the menu frame
        if (findViewById(R.id.menu_frame) == null) {
            setBehindContentView(R.layout.menu_frame);
            getSlidingMenu().setSlidingEnabled(true);
            getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
            // show home as up so we can toggle
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } else {
            // add a dummy view
            View v = new View(this);
            setBehindContentView(v);
            getSlidingMenu().setSlidingEnabled(false);
            getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        }

        // set the Above View Fragment
        if (savedInstanceState != null)
            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
        if (mContent == null)
            mContent = new PollPetitionsWrapperFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, mContent)
                .commit();

        // set the Behind View Fragment
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.menu_frame, new SlideMenuFragment())
                .commit();

        // customize the SlidingMenu
        SlidingMenu sm = getSlidingMenu();
        sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        sm.setShadowWidthRes(R.dimen.shadow_width);
        sm.setShadowDrawable(R.drawable.shadow);
        sm.setBehindScrollScale(0.25f);
        sm.setFadeDegree(0.25f);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                toggle();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "mContent", mContent);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        getSupportMenuInflater().inflate(R.menu.activity_main, menu);
        MenuItem categoriesItem = menu.findItem(R.id.categories);
        MenuItem updateItem     = menu.findItem(R.id.update);

        updateItem.setVisible(!isHideOptionsMenu());
        SubMenu subMenu = categoriesItem.getSubMenu();
        String[] items = getResources().getStringArray(R.array.categories_names);
        for(int i =0; i < items.length; i++) {
            subMenu.add(1, i, i, items[i]);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    public void switchContent(final Fragment fragment) {
        mContent = fragment;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            public void run() {
                getSlidingMenu().showContent();
            }
        }, 50);
    }

    public boolean isHideOptionsMenu() {
        return isHideOptionsMenu;
    }

    public void setHideOptionsMenu(boolean state) {
        isHideOptionsMenu = state;
    }

    @Override
    public void setSupportProgressBarIndeterminateVisibility(boolean visible) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            setHideOptionsMenu(visible);
            supportInvalidateOptionsMenu();
            super.setSupportProgressBarIndeterminateVisibility(visible);
        }
    }

    @Override
    public void requestWindowFeature(long featureId) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            super.requestWindowFeature((int)featureId);
        }
    }
}
