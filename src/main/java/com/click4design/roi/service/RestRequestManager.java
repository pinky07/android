package com.click4design.roi.service;

import android.content.Context;
import com.foxykeep.datadroid.requestmanager.RequestManager;

/**
 * Author: Khamidullin Kamil
 * Date: 20.06.13
 * Time: 0:40
 */
public class RestRequestManager extends RequestManager {
    private RestRequestManager(Context context) {
        super(context, RestService.class);
    }

    private static RestRequestManager sInstance;

    public static RestRequestManager from(Context context) {
        if (sInstance == null) {
            sInstance = new RestRequestManager(context);
        }
        return sInstance;
    }
}
