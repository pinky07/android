package com.click4design.roi.service;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import com.click4design.roi.model.Petition;
import com.click4design.roi.network.RoiClient;
import com.click4design.roi.persist.PersistHelper;
import com.click4design.roi.provider.Contract;
import com.foxykeep.datadroid.exception.ConnectionException;
import com.foxykeep.datadroid.exception.CustomRequestException;
import com.foxykeep.datadroid.exception.DataException;
import com.foxykeep.datadroid.requestmanager.Request;
import com.foxykeep.datadroid.service.RequestService;
import org.apache.http.client.HttpClient;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.client.UrlConnectionClient;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 20.06.13
 * Time: 0:08
 */
public final class PetitionsOperation implements RequestService.Operation {

    @Override
    public Bundle execute(Context context, Request request)
            throws ConnectionException, DataException, CustomRequestException {
        RestAdapter adapter = new RestAdapter.Builder()
                .setServer(RoiClient.API_URL)
                .setClient(new UrlConnectionClient())
                .build();

        RoiClient client = adapter.create(RoiClient.class);

        String category = request.getString("category");
        String sort     = request.getString("sort");
        int status      = request.getInt("status");
        int limit       = request.getInt("limit");
        int offset      = request.getInt("offset");
        String ids      = PersistHelper.getWatchListAsString(context);

        List<Petition> petitions;
        try {
            if(status == 0) {
                petitions = client.petitions(ids, sort, limit, offset);
                final List<String> watchList = PersistHelper.getWatchList(context);
                Collections.sort(petitions,  new Comparator<Petition>() {
                    @Override
                    public int compare(Petition lhs, Petition rhs) {
                        int lhsOrder = 0;
                        int rhsOrder = 0;

                        for(int i = 0; i < watchList.size(); i++) {
                            String id = watchList.get(i);
                            if(id.equals(lhs.getId()))
                                lhsOrder = i;
                            else if(id.equals(rhs.getId()))
                                rhsOrder = i;
                        }

                        return lhsOrder-rhsOrder;
                    }
                });
            } else {
                petitions = (category == null) ?
                        client.petitions(sort, status, limit, offset):
                        client.petitions(category, sort, status, limit, offset);
            }
        } catch (Exception ex) {
             throw new DataException();
        }


        Bundle bundle = new Bundle();
        bundle.putParcelableArray(
                "petitions",
                petitions.toArray(new Petition[petitions.size()])
        );

        return bundle;
    }
}
