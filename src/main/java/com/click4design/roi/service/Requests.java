package com.click4design.roi.service;

/**
 * Author: Khamidullin Kamil
 * Date: 20.06.13
 * Time: 0:04
 */
public interface Requests {
    public final static int CATEGORIES = 111;
    public final static int PETITIONS = 112;
    public final static int PETITION = 113;
}
