package com.click4design.roi.service;

import com.foxykeep.datadroid.service.RequestService;

/**
 * Author: Khamidullin Kamil
 * Date: 20.06.13
 * Time: 0:03
 */
public class RestService extends RequestService {

    @Override
    public RequestService.Operation getOperationForType(int requestType) {
        switch (requestType) {
            case Requests.PETITIONS:
                return new PetitionsOperation();
            case Requests.CATEGORIES:
                return new CategoriesOperation();
            default:
                return null;
        }
    }
}
