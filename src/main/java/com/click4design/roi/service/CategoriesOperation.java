package com.click4design.roi.service;

import android.content.Context;
import android.os.Bundle;
import com.click4design.roi.model.Category;
import com.click4design.roi.network.RoiClient;
import com.foxykeep.datadroid.exception.ConnectionException;
import com.foxykeep.datadroid.exception.CustomRequestException;
import com.foxykeep.datadroid.exception.DataException;
import com.foxykeep.datadroid.requestmanager.Request;
import com.foxykeep.datadroid.service.RequestService;
import retrofit.RestAdapter;
import retrofit.client.UrlConnectionClient;

import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 21.06.13
 * Time: 2:50
 */
public final class CategoriesOperation implements RequestService.Operation {
    @Override
    public Bundle execute(Context context, Request request)
            throws ConnectionException, DataException, CustomRequestException {
        RestAdapter adapter = new RestAdapter.Builder()
                .setServer(RoiClient.API_URL)
                .setClient(new UrlConnectionClient())
                .build();

        RoiClient client = adapter.create(RoiClient.class);
        try {
            List<Category> categories = client.categories();
            Bundle bundle = new Bundle();
            bundle.putParcelableArray(
                    "categories",
                    categories.toArray(new Category[categories.size()])
            );

            return bundle;
        }  catch (Exception e) {
            e.getCause();
            throw new DataException(e.getMessage());
        }

    }
}
