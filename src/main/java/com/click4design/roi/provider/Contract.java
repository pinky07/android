package com.click4design.roi.provider;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Author: Khamidullin Kamil
 * Date: 20.06.13
 * Time: 0:22
 */
public final class Contract {
    public static final String AUTHORITY = "ru.click4design.roi";

    public static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);

    public interface PetitionsCoulmns {
        public static final String ID = "id";
        public static final String TITLE = "body";
        public static final String VOTES = "votes";
    }

    public static final class Petitions implements BaseColumns, PetitionsCoulmns {
        public static final String CONTENT_PATH = "petitions";
        public static final Uri CONTENT_URI = Uri.withAppendedPath(AUTHORITY_URI, CONTENT_PATH);
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd." + AUTHORITY + "." + CONTENT_PATH;
    }
}
