package com.click4design.roi.persist;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.click4design.roi.model.Petition;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Khamidullin Kamil
 * Date: 23.06.13
 * Time: 12:20
 */
public class PersistHelper {
    protected final static String WATCHES_KEY = "watches";

    public static void watchPetition(Context context, Petition petition) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            String jsonString = sPref.getString(WATCHES_KEY, "");
            JSONArray watches = TextUtils.isEmpty(jsonString) ? new JSONArray() : new JSONArray(jsonString);
            for(int i = 0; i < watches.length(); i++) {
                if(watches.getString(i).equals(petition.getId()))
                    return;
            }
            watches.put(petition.getId());
            SharedPreferences.Editor ed = sPref.edit();
            ed.putString(WATCHES_KEY, watches.toString());
            ed.commit();
        } catch (JSONException ex) {
            //
        }
    }

    public static boolean isWatchPetition(Context context, Petition petition) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            JSONArray watches = new JSONArray(sPref.getString(WATCHES_KEY, ""));
            for(int i = 0; i < watches.length(); i++) {
                if(watches.getString(i).equals(petition.getId()))
                    return true;
            }

            return false;
        } catch (JSONException ex) {
            return false;
        }
    }

    public static void unWatch(Context context, Petition petition) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            JSONArray watches = new JSONArray(sPref.getString(WATCHES_KEY, ""));
            ArrayList<String> list = new ArrayList<String>();
            for(int i = 0; i < watches.length(); i++) {
                if(!watches.getString(i).equals(petition.getId()))
                    list.add(watches.getString(i));
            }
            SharedPreferences.Editor ed = sPref.edit();
            ed.putString(WATCHES_KEY, new JSONArray(list).toString());
            ed.commit();

        } catch (JSONException ex) {
            //
        }
    }

    public static List<String> getWatchList(Context context) {
        ArrayList<String> watchList = new ArrayList<String>();
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(context);
        try {
            JSONArray watches = new JSONArray(sPref.getString(WATCHES_KEY, ""));
            for(int i = 0; i < watches.length(); i++) {
                watchList.add(watches.getString(i));
            }

        } catch (JSONException ex) {
            //
        }

        return watchList;
    }

    public static String getWatchListAsString(Context context) {
        String output = "";
        List<String> watchList = getWatchList(context);
        if (watchList.size() > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(watchList.get(0));
            for (int i = 1; i < watchList.size(); i++) {
                sb.append(":");
                sb.append(watchList.get(i));
            }
            output = sb.toString();
        }
        return output;
    }
}
